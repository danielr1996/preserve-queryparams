document.addEventListener("DOMContentLoaded", () => {
    Array.from(document.querySelectorAll("a[rel~='preserve-queryparams']")).forEach(anchor => {
        anchor.addEventListener('click', (e) => {
            e.preventDefault();
            let target: any = e.target;
            var params = window.location.search;
            var dest = params+target.getAttribute('href');
            window.location.href = dest;
        });
    });
});