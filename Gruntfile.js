module.exports = function(grunt){
    grunt.initConfig({
        exec: {
            publish: 'npm publish --access public'
        },
        webpack: {
            build: require('./webpack.config')
        }
    });
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-webpack');
    grunt.registerTask('pub', ['exec:publish']);
    grunt.registerTask('default', ['webpack']);
};